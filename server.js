const express = require('express');

var app = express();

app.get('/', (req, res) => {
  res.send('Hello Mr. Bond!');
});

app.listen(8080, () => {
  console.log(`Server up and running on port 8080`);
});
